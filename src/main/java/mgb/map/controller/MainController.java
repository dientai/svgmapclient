package mgb.map.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by dtlinh on 12/29/2015.
 */
@Controller
public class MainController {

    @RequestMapping(value = {"/"})
    public String index(Model model) {
        return "index";
    }

    @RequestMapping(value = {"/map"})
    public String map(Model model) {
        return "map";
    }

    @RequestMapping(value = {"svgmap/{lang}/{season}"})
    public String season(Model model) {
        return "index";
    }

    @ResponseBody
    @RequestMapping(value = {"/getMapTypes"})
    public String getMapTypes() {
        return "";
    }

}
