(function ( $ ) {
    $.fn.mgb = function(opts) {
        // This is the easiest way to have default options.
        var self = this;
        var $mapArea;
        var $svg;
        var currentScale = 1;

        var settings = $.extend({
            // These are the defaults.
            mapType: "summer",
            lang: "en",
            maxScale: 6
        }, opts );

        self.append("<div class='map-area'></div>");
        $mapArea = $(".map-area");
        if (settings.mapType == "summer") {
            var summerImage = "<img src=\"" + CONTEXT_ROOT + "img/summer-bg.png\" alt=\"\" width=\"2000\" height=\"1090\">";
            $mapArea.append(summerImage);
        } else {
            var winterImage = "<img src=\"" + CONTEXT_ROOT + "img/winter-bg.png\" alt=\"\" width=\"2000\" height=\"1090\">";
            $mapArea.append(winterImage);
        }

        function initPanZoom($element){
            $element.panzoom({
                contain: "invert",
                minScale: 1,
                maxScale: settings.maxScale,
                easing: "ease-in-out",
                transition: true,
                duration: 200,
                startTransform: "matrix(1, 0, 0, 1, 0, 0)",
                $zoomIn: $(".zoom-in"),
                $zoomOut: $(".zoom-out")
            });

            $element.on({
                "panzoompan" : function() {
                    if ( currentScale >= 1 ) {
                        $svg.hide();
                    }
                },
                "panzoomend" : function() {
                    if ( currentScale >= 1 ) {
                        $svg.show();
                    }
                },
                "panzoomzoom": function(e, panzoom, scale){
                    currentScale = scale;
                }
            });

            $(".adjust").click(function(ev) {
                $element.panzoom("reset");
                currentScale = 1;
                ev.preventDefault();
            });
        }

        $(window).resize(function() {
            if(window.outerWidth < 768) {
                $mapArea.panzoom("destroy");
                initPanZoom($mapArea);
            }

        });

        $.ajax({
            method: "GET",
            url: CONTEXT_ROOT + "img/" + settings.mapType + ".svg",
            async:false,
            dataType: "text",
            data: {
                lang: settings.lang,
                season: settings.mapType
            },
            beforeSend: function () {
            },
            success: function(data) {
                $mapArea.append(data);
                self.append('<div class="svg-actions">' +
                    '<div class="btn zoom-in"><div>&nbsp;</div></div>' +
                    '<div class="btn zoom-out"><div>&nbsp;</div></div>' +
                    '<div class="btn adjust"><div>&nbsp;</div></div>' +
                    '<div class="clear"></div>' +
                    '</div>');

                $svg = $("svg");
                $("svg #Background").remove();
                initPanZoom($mapArea);
            }
        });

        return this;
    };
}( jQuery ));

