/**
 * Created by tvdien on 31-Dec-15.
 */

var defaultLang = 'en';
var defaultSeason = 'summer';
var currentSelectedPath = null;

var svgObjectAttribute = [{
    "svgId": "Sig_Wandern_3",
    "objectTypeName": "hikingtrailicon",
    "attributes": [{
        "attributeName": "hovername",
        "value": {
            "en": "Marmot walk 2",
            "de": "Murmelweg",
            "fr": ""
        }
    }, {
        "attributeName": "title",
        "value": {
            "en": "Marmot walk title 2",
            "de": "Murmelweg für die Familie",
            "fr": ""
        }
    }, {
        "attributeName": "text",
        "value": {
            "en": "Themed hiking trail from Sunnegga to Blauherd 2",
            "de": "Themenwanderweg von Sunnegga zum Blauherd",
            "fr": ""
        }
    }, {
        "attributeName": "distance",
        "value": "3.8km"
    }, {
        "attributeName": "time",
        "value": "1:30"
    }]
}, {
    "svgId": "Sig_Wandern_4",
    "objectTypeName": "hikingtrailicon",
    "attributes": [{
        "attributeName": "hovername",
        "value": {
            "en": "Marmot walk 2",
            "de": "Murmelweg",
            "fr": ""
        }
    }, {
        "attributeName": "title",
        "value": {
            "en": "Marmot walk title 2",
            "de": "Murmelweg für die Familie",
            "fr": ""
        }
    }, {
        "attributeName": "text",
        "value": {
            "en": "Themed hiking trail from Sunnegga to Blauherd 2",
            "de": "Themenwanderweg von Sunnegga zum Blauherd",
            "fr": ""
        }
    }, {
        "attributeName": "distance",
        "value": "3.8km"
    }, {
        "attributeName": "time",
        "value": "1:30"
    }]
}, {
    "svgId": "Sig_Wandern_1",
    "objectTypeName": "hotelicon",
    "attributes": [{
        "attributeName": "hovername",
        "value": {
            "en": "Marmot walk 1",
            "de": "Murmelweg",
            "fr": ""
        }
    }, {
        "attributeName": "title",
        "value": {
            "en": "Marmot walk 1",
            "de": "Murmelweg für die Familie",
            "fr": ""
        }
    }, {
        "attributeName": "text",
        "value": {
            "en": "Themed hiking trail from Sunnegga to Blauherd 1",
            "de": "Themenwanderweg von Sunnegga zum Blauherd",
            "fr": ""
        }
    }, {
        "attributeName": "distance",
        "value": "3.8km"
    }, {
        "attributeName": "time",
        "value": "1:30"
    }]
}, {
    "svgId": "Wanderwege_1",
    "objectTypeName": "railwaypath",
    "attributes": [{
        "attributeName": "hovername",
        "value": {
            "en": "Marmot walk 1",
            "de": "Murmelweg",
            "fr": ""
        }
    }, {
        "attributeName": "title",
        "value": {
            "en": "Marmot walk path 1",
            "de": "Murmelweg für die Familie",
            "fr": ""
        }
    }, {
        "attributeName": "text",
        "value": {
            "en": "Themed hiking trail from Sunnegga to Blauherd 1",
            "de": "Themenwanderweg von Sunnegga zum Blauherd",
            "fr": ""
        }
    }, {
        "attributeName": "distance",
        "value": "3.8km"
    }, {
        "attributeName": "time",
        "value": "1:30"
    }]
}, {
    "svgId": "Wanderwege_2",
    "objectTypeName": "railwaypath",
    "attributes": [{
        "attributeName": "hovername",
        "value": {
            "en": "Marmot walk 2",
            "de": "Murmelweg",
            "fr": ""
        }
    }, {
        "attributeName": "title",
        "value": {
            "en": "Marmot walk path 2",
            "de": "Murmelweg für die Familie",
            "fr": ""
        }
    }, {
        "attributeName": "text",
        "value": {
            "en": "Themed hiking trail from Sunnegga to Blauherd 2",
            "de": "Themenwanderweg von Sunnegga zum Blauherd",
            "fr": ""
        }
    }, {
        "attributeName": "distance",
        "value": "3.8km"
    }, {
        "attributeName": "time",
        "value": "1:30"
    }]
}];

$(function () {
    // remove bubble info by clicking outside
    closeBubbleInfo();
    // check url for summer or winter
    getDefaultConfig();
    getFileSVGMap();
    // get list map session
    getListMapSeasonType();
    // listener for select box
    addListenerForSelectBox();
});

function addEventForIcon(svgId) {
    svgId.click(function (e) {
        $(this).off('mouseleave');
        $('.info_panel').remove();
        var mouseX = e.pageX; // X coordinates of mouse
        var mouseY = e.pageY; // Y coordinates of mouse
        var objAttr = $(this).data('objAttr');
        var infoDiv = addInfoToDivByAttr(objAttr.attributes);
        addBubbleInfo(infoDiv);
        setBubblePosition(mouseX, mouseY);
        addCloseBubbleEvent();
        e.stopPropagation();
    }).hover(function (e) {
        resetPathCss(currentSelectedPath);
        var objAttr = $(this).data('objAttr');
        var hasHover = hasHoverAttribute(objAttr.attributes);
        if (hasHover != '') {
            $('.info_panel').remove();
            var mouseX = e.pageX; // X coordinates of mouse
            var mouseY = e.pageY; // Y coordinates of mouse
            var infoDiv = addHoverInfoToDivByAttr(hasHover);
            addBubbleInfo(infoDiv, true);
            setBubblePosition(mouseX, mouseY);
            addCloseBubbleEvent();

            $(this).mouseleave(function () {
                $('.info_panel').remove();
            });
        }
    });
}

function addEventForPath(svgId) {
    svgId.click(function (e) {
        currentSelectedPath = $(this).find('path');
        $(this).off('mouseleave');
        $('.info_panel').remove();
        var mouseX = e.pageX; // X coordinates of mouse
        var mouseY = e.pageY; // Y coordinates of mouse
        var objAttr = $(this).data('objAttr');
        var infoDiv = addInfoToDivByAttr(objAttr.attributes);
        addBubbleInfo(infoDiv);
        setBubblePosition(mouseX, mouseY);
        $('.info_close').data('thisPathId', $(this).find('path'));
        addCloseBubbleEvent(true);
        addHoverPathCss($(this).find('path'));
        e.stopPropagation();
    }).hover(function (e) {
        resetPathCss(currentSelectedPath);
        addHoverPathCss($(this).find('path'));
        var objAttr = $(this).data('objAttr');
        var hasHover = hasHoverAttribute(objAttr.attributes);
        if (hasHover != '') {
            $('.info_panel').remove();
            var mouseX = e.pageX; // X coordinates of mouse
            var mouseY = e.pageY; // Y coordinates of mouse
            var infoDiv = addHoverInfoToDivByAttr(hasHover);
            addBubbleInfo(infoDiv, true);
            setBubblePosition(mouseX, mouseY);
            addCloseBubbleEvent();
        }

        $(this).mouseleave(function () {
            resetPathCss($(this).find('path'));
            $('.info_panel').remove();
        });
    });
}

function addBubbleInfo(infoDiv, isHover) {
    var bubbleDiv = '<div class="info_panel">';
    if (!isHover) {
        bubbleDiv += '<div class="info_close"></div>';
    }
    bubbleDiv += '<div class="info_content">' + infoDiv + '</div>' + '<div class="down-arrow"></div>' + '</div>';
    $(bubbleDiv).appendTo('body');
}

function setBubblePosition(mouseX, mouseY) {
    $('.info_panel').css({
        top: mouseY - $('.info_panel').height() - 30,
        left: mouseX - ($('.info_panel').width() / 1.85)
    });
}

function addCloseBubbleEvent(isPathIcon) {
    if (isPathIcon) {
        $('.info_close').click(function () {
            var thisPathId = $(this).data('thisPathId')
            $('.info_panel').remove();
            resetPathCss(thisPathId);
        });
    } else {
        $('.info_close').click(function () {
            $('.info_panel').remove();
        });
    }
}

function addHoverPathCss(element) {
    element.css({'stroke-width': '2'});
}

function resetPathCss(element) {
    if (element != null) {
        element.css({'stroke-width': '1'});
    }
}

function addInfoToDivByAttr(obj) {
    var strHTML = '';
    for (var i = 0; i < obj.length; i++) {
        if (obj[i].attributeName == 'title') {
            strHTML += '<div class="info_title">' + obj[i].value.en + '</div>';
        }
        if (obj[i].attributeName == 'text') {
            strHTML += '<div class="info_text">' + obj[i].value.en + '</div>';
        }
        if (obj[i].attributeName == 'distance') {
            strHTML += '<div class="info_text">' + obj[i].value + '</div>';
        }
        if (obj[i].attributeName == 'time') {
            strHTML += '<div class="info_text">' + obj[i].value + '</div>';
        }
    }
    return strHTML;
}

function addHoverInfoToDivByAttr(value) {
    return '<div class="info_text">' + value + '</div>';
}

function hasHoverAttribute(obj) {
    for (var i = 0; i < obj.length; i++) {
        if (obj[i].attributeName == 'hovername') {
            return obj[i].value.en;
        }
    }
    return '';
}

function getListMapSeasonType() {
    $.ajax({
        method: "GET",
        url: CONTEXT_ROOT + "getMapTypes",
        data: {},
        beforeSend: function () {
        }
    }).done(function (data) {
        if (data != null) {
            createMapSeasonOption(data);
        }
    }).fail(function (status) {
        console.log('Get list map type failed for following reason: ' + status);
    }).always(function () {
    });
}

function createMapSeasonOption(opts) {
    $.each(opts, function (k, obj) {
        var txt = getOptionSeasonName(defaultLang, obj.displayName);
        var option = $('<option/>');
        option.attr({'value': obj.mapName}).text(txt);
        if (option.attr('value') == defaultSeason) {
            option.prop('selected', true);
        }
        $('.season-select').append(option);
    });
}

function getOptionSeasonName(dlang, names) {
    var dName = "";
    $.each(names, function (k, v) {
        if (dlang === k) {
            dName = v;
            return false;
        }
    });
    return dName;
}

function getDefaultConfig() {
    var url = document.location.pathname;
    if (url.indexOf('en') > -1) {
        defaultLang = 'en';
    } else if (url.indexOf('de') > -1) {
        defaultLang = 'de';
    } else if (url.indexOf('fr') > -1) {
        defaultLang = 'fr';
    }

    if (url.indexOf('summer') > -1 || url.indexOf('ete') > -1 || url.indexOf('sommer') > -1) {
        defaultSeason = 'summer';
    } else if (url.indexOf('winter') > -1 || url.indexOf('hiver') > -1) {
        defaultSeason = 'winter';
    }

    $('.season-select option').each(function (index, option) {
        if (option.value == defaultSeason) {
            $(this).prop('selected', true);
        }
    });
}

function getFileSVGMap() {
    $.ajax({
        method: "GET",
        url: CONTEXT_ROOT + "img/" + defaultSeason + ".svg",
        dataType: "text",
        data: {
            lang: defaultLang,
            season: defaultSeason
        },
        beforeSend: function () {
        }
    }).done(function (data) {
        if (data != null) {
            setBackgroundImageBySeason();
            appendSVGMapSource(data);
            addEventForSVGMap();
        }
    }).fail(function (status) {
        console.log('Get list map type failed for following reason: ' + status);
    }).always(function () {
    });
}

function setBackgroundImageBySeason() {
    $('#svg-container').css({'background-image': 'url(../../img/' + defaultSeason + '-bg.png)'});
}

function appendSVGMapSource(data) {
    $('#svg-container').append(data);
}

function addEventForSVGMap() {
    for (var i = 0; i < svgObjectAttribute.length; i++) {
        var objAttr = svgObjectAttribute[i];
        console.log('svgId==== ' + objAttr.svgId);
        $("#" + objAttr.svgId).data('objAttr', objAttr);
        if (objAttr.objectTypeName.indexOf('path') == -1) {
            addEventForIcon($("#" + objAttr.svgId));
        } else {
            addEventForPath($("#" + objAttr.svgId));
        }
    }
}

function addListenerForSelectBox() {
    $('.season-select').change(function () {
        var thisVal = $(this).find('option:selected').attr('value');
        if (thisVal == 'summer') {
            var season = 'summer';
            if (defaultLang == 'fr') {
                season = 'ete';
            } else if (defaultLang == 'de') {
                season = 'sommer';
            }
            document.location.href = CONTEXT_ROOT + 'svgmap/' + defaultLang + '/' + season;
        } else {
            var season = 'winter';
            if (defaultLang == 'fr') {
                season = 'hiver';
            }
            document.location.href = CONTEXT_ROOT + 'svgmap/' + defaultLang + '/' + season;
        }
    });
}

function closeBubbleInfo() {
    $('body').click(function () {
        $('.info_panel').remove();
        resetPathCss(currentSelectedPath);
    });
}